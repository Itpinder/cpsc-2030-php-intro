<!DOCTYPE html>
<html>
  <head>
    <title>Checkout confirmation</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  
  <body>
    <div>
        <!--BILLING ADDRESS STARTS HERE-->
        <div class="col-md-5 order-md-2 mb-4">
            <h4 class="d-block justify-content-between align-items-center mb-3">
                <span class="text-muted">Your Billing Address</span>
            </h4>
           
            <ul class="list-group mb-3 container-fluid">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Name: </h6>
                    <?php echo $_POST["firstName"]. " ".$_POST["lastName"]?>
                  
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Email: </h6>
                     <?php
                        $email= $_POST["email"];
                        if($email != ""){
                            print nl2br("$email\n");
                        }
                        else{
                            echo "";
                        }
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Address: </h6>
                    <?php
            
            
                        if(isset($_POST["province"])){
                         foreach($_POST["province"] as $province){
                             $provinceSelected= $province;
                         }
                        }
                        
                        
                        if(isset($_POST["country"])){
                         foreach($_POST["country"] as $country){
                             $countrySelected= $country;
                         }
                        }
                        
                        
                        $postalCode= $_POST["postalCode"];
                            
                        echo $_POST["address"]. ", ". $provinceSelected. ", ". $countrySelected. ", ". $postalCode
                    ?>
                </li>
                
         
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Address2: </h6>
                    <?php 
                            $address2= $_POST["address2"];
                            if($address2 != ""){
                                
                                print nl2br("$address2, $provinceSelected, $countrySelected, $postalCode\n");
                            }
                            else{
                                echo "";
                            }
                    ?>
                </li>
        
       
        </ul>
    </div>
        <!--BILLING ADDRESS ENDS HERE-->
        
        
        <!--MODE OF PAYMENT STARTS HERE-->
        
    <div class="col-md-5 order-md-2 mb-4">
        <h4 class="d-block justify-content-between align-items-center mb-3">
            <span class="text-muted">Payment info</span>
        </h4>
        
        <ul class="list-group mb-3 container-fluid">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <h6 class="my-0">Mode of payment: </h6>
                <?php
                    $paymentMethod= $_POST["paymentMethod"];
                    echo $paymentMethod;
                ?>
            </li>
            
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <h6 class="my-0">Name on the card: </h6>
                <?php
                     $nameOnCard= $_POST["cc-name"];
                    echo $nameOnCard;
                ?>
            </li>
            
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <h6 class="my-0">Card number: </h6>
                <?php
                     $cardNumber= $_POST["cc-number"];
                    echo $cardNumber;
                ?>
            </li>
            
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <h6 class="my-0">Card expiry date: </h6>
                <?php
                    $cardExpiry= $_POST["cc-expiration"];
                    echo $cardExpiry;
                ?>
            </li>
            
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <h6 class="my-0">Card CVV: </h6>
                <?php
                    $cardCVV= $_POST["cc-cvv"];
                    echo $cardCVV;
                ?>
            </li>
        </ul>
        
    </div>
        
          <!--MODE OF PAYMENT ENDS HERE-->
          
         <!--CREDIT CARD LIMIT-->
        <?php
            $creditLimit= 750;
        ?>
        
            
        <!--TOTAL CHARGES BEFORE TAX    -->

        <div class="col-md-5 order-md-2 mb-4">
            <h4 class="d-block justify-content-between align-items-center mb-3">
                <span class="text-muted">Charges before tax</span>
            </h4>
            
            <h5 class="d-block justify-content-between align-items-center mb-3">
                <span class="text-muted">Your cart</span>
            </h5>
            
            <table class="table">
                
                <!--HEADING-->
                <tr>
                    <th></th>
                    <th>Quantity</th>
                    <th>Price per item</th>
                    <th>Total cost of the item</th>
                </tr>
                
                <!--FIRST ROW-->
                <tr>
                    <th>First Product</th>
                    <td><?php
                             $quantityFirst= $_POST["quantityFirst"];
                             print("$quantityFirst");
                        ?>
                    </td>
                    <td><?php
                            $priceFirst= 12;
                            print("\$$priceFirst");
                        ?>
                    </td>
                    <td><?php
                            $priceOfFirstProduct= $quantityFirst* $priceFirst;
                            print("\$$priceOfFirstProduct\n");
                        ?>
                    </td>
                </tr>
                
                
                <!--SECOND ROW-->
                <tr>
                    <th>Second Product</th>
                    <td><?php
                             $quantitySecond= $_POST["quantityFirst"];
                             print("$quantitySecond");
                        ?>
                    </td>
                    <td><?php
                            $priceSecond= 8;
                            print("\$$priceSecond");
                        ?>
                    </td>
                    <td><?php
                            $priceOfSecondProduct= $quantitySecond* $priceSecond;
                            print("\$$priceOfSecondProduct\n");
                        ?>
                    </td>
                </tr>
                
                
                <!--THIRD ROW-->
                <tr>
                    <th>Third Product</th>
                    <td><?php
                             $quantityThird= $_POST["quantityThird"];
                             print("$quantityThird");
                        ?>
                    </td>
                    <td><?php
                            $priceThird= 5;
                            print("\$$priceThird");
                        ?>
                    </td>
                    <td><?php
                            $priceOfThirdProduct= $quantityThird* $priceThird;
                            print("\$$priceOfThirdProduct\n");
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <th>Total of cart: </th>
                    <td></td>
                    <td></td>
                    <td>
                         <?php
                            $totalPriceOfTheProducts= $priceOfFirstProduct+ $priceOfSecondProduct+ $priceOfThirdProduct;
                            print("\$$totalPriceOfTheProducts");
                        ?>
                    </td>
                </tr>
            </table>
            
            <h5 class="d-block justify-content-between align-items-center mb-3">
                <span class="text-muted">Other charges</span>
            </h5>
            
            <h6>
                <?php
                    $giftWrap= 20;
                    $deliveryCharge;
                    
                    if(isset($_POST["giftWrap"])){
                       print nl2br("Charges for gift wrap are: \$$giftWrap\n");
                    }
                    
                    if($_POST["delivery"]== "Mail"){
                        $deliveryCharge= 15;
                        print("Charges for mail are: \$$deliveryCharge");
                    }
                    else if($_POST["delivery"]== "Courier"){
                        $deliveryCharge= 30;
                        print("Charges for courier are: \$$deliveryCharge");
                    }
                    else if($_POST["delivery"]== "Pick up"){
                        $deliveryCharge= 10;
                        print("Charges for pick-up are: \$$deliveryCharge");
                    }
                ?>
            </h6>
            
            
        </div>
            
            <hr>
            <hr>
            
            <!--OVERALL CHARGES BEFORE TAX-->
            <h5 class="d-block justify-content-between align-items-center mb-3">
                <span class="text-muted">Overall charges before tax(Subtotal): </span>
                <?php
                    $totalBeforeTax= $totalPriceOfTheProducts+ $giftWrap+ $deliveryCharge;
                    print nl2br("\$$totalBeforeTax\n");
                ?>
            </h5>
            
            
            <!--TAXES CALCULATION STARTS HERE-->
        <?php 
            $totalTax;
            if($provinceSelected== "Alberta"){
                $tax= 5;
            }
            if($provinceSelected== "British Columbia"){
                $tax= 12;
            }
            if($provinceSelected== "Manitoba"){
                $tax= 13;
            }
            if($provinceSelected== "New Brunswick"){
                $tax= 15;
            }
            if($provinceSelected== "Newfoundland and Labrador"){
                $tax= 15;
            }
            if($provinceSelected== "Northwest Territories"){
                $tax= 5;
            }
            if($provinceSelected== "Nova Scotia"){
                $tax= 15;
            }
            if($provinceSelected== "Nunavut"){
                $tax= 5;
            }
            if($provinceSelected== "Ontario"){
                $tax= 13;
            }
            if($provinceSelected== "Prince Edward"){
                $tax= 15;
            }
            if($provinceSelected== "Quebec"){
                $tax= 14.975;
            }
            if($provinceSelected== "Saskatchewan"){
                $tax= 11;
            }
            if($provinceSelected== "Yukon"){
                $tax= 5;
            }
        ?>
       
        <h5 class="d-block justify-content-between align-items-center mb-3">
            <span class="text-muted">Tax: </span>
            <?php
                 $totalTax= $totalBeforeTax*($tax/100);
                 print("\$$totalTax");
            ?>
        </h5>
        
        <h5 class="d-block justify-content-between align-items-center mb-3">
            <span class="text-muted">Total after tax(Grand Total): </span>
            <?php
                 $grandTotal= $totalBeforeTax+ $totalTax;
                 
                 if($grandTotal> $creditLimit){
                   
                    $message = "Sorry, your credit limit exceeded. Payment declined!!!";
                    echo "<script type='text/javascript'>alert('$message');</script>";
                    
                     
                 }
                 
                  print nl2br("\$$grandTotal");
            ?>
        </h5>
         <!--TAXES CALCULATION ENDS HERE-->
    </div>
    
      
      
  </body>
</html>
