<?php
  session_start();
 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
  </head>
  
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
    
    <form class="needs-validation" novalidate method="POST" action="">
      <button class="btn btn-primary btn-lg btn-block" type="submit" name="login">Click here if you want to add or delete a record</button>
    </form>
  
    <?php
      if(isset($_POST["login"])){
        header("Location: https://92c7c3a7534d4e0ca0a17e756a858779.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/database-sessions/login.php/");
      }
    ?>
  
  


<!--FORM TO ADD A RECORD-->

    <?php if ( $_SESSION["loggedin"]== true ) { ?>
    
      <form class="needs-validation" novalidate method="POST" action="">
        <div class="row">
            <!--MAKE-->
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <!--MODEL-->
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <!--YEAR-->
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
            <input type="number" class="form-control" id="year" placeholder="" value="" required name="year">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <!--MILAGE-->
          <div class="col-md-6 mb-3">
            <label for="mileage">Mileage</label>
            <input type="number" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
      </form>
      
      
  <!--FORM TO DELETE A RECORD-->
      <form class="needs-validation" novalidate method="POST" action="">
        <hr>
        <hr>
        <div class="col-md-6 mb-3">
              <label for="id">Enter id of the record that you want to delete</label>
              <input type="number" class="form-control" id="id" placeholder="" value="" name="id">
            </div>
        
        <button class="btn btn-primary btn-lg btn-block hidden" type="submit">Delete car</button>
      </form>
      
      
      <form class="needs-validation" novalidate method="POST" action="">
        <hr>
        <hr>
      
        <button class="btn btn-danger btn-lg hidden" type="submit" name="logout">Logout</button>
      </form>
      <?php } ?>


      <table class="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Mileage</th>
          </tr>
        </thead>
          <tbody>
            
              <!--PHP SCRIPT GOES HERE-->
            <?php
              $link = mysqli_connect( 'localhost', 'root', 'Sukhman8*' );       // local host, username, password
              mysqli_select_db( $link, 'demo' );                     // demo==> database name
                
              // code to add record
              // IF SOMEBODY FILLED OUT THAT FORM AND CLICKED ON ADD CAR BUTTON, THE VALUE FOR "isset( $_REQUEST["make"] )" IS GONNA BE "True"
              if (isset( $_REQUEST["make"] )) {
                $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
                $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                $safe_year= mysqli_real_escape_string($link, $_REQUEST["year"]);
                $safe_mileage= mysqli_real_escape_string($link, $_REQUEST["mileage"]);
                
                // TO CHECK IF THE DATA ENTERED IS IN RIGHT TYPE
                if((!preg_match("/^[A-Za-z]$+/i", $safe_make)) || (!preg_match("/^[A-Za-z]$+/i", $safe_model)) || (!preg_match("/^[0-9]+$/", $safe_year)) || (!preg_match("/^[0-9]+$/", $safe_mileage))){
                  echo "ENTER THE INFORMATION IN CORRECT FORMAT!!!";
                }
                else{
                  $query = "INSERT INTO cars ( Make, Model, Year, Mileage ) VALUES ( '$safe_make', '$safe_model', '$safe_year', '$safe_mileage' )";
                  
                  // TO CHECK THE QUERY
                  if(!$query){
                    print(mysqli_error($link));
                  }
                  else{
                    mysqli_query( $link, $query );
                  }
                }

              }
                
              // DELETE A RECORD BY ID
              if(isset($_POST["id"])){
                $safe_id= mysqli_real_escape_string( $link, $_REQUEST["id"] );
                $query= "DELETE FROM cars WHERE id= $safe_id";
                mysqli_query( $link, $query ); 
              }
              
              $results = mysqli_query( $link, 'SELECT * FROM cars' );
              
              // PRINT RESULTS
              while( $record = mysqli_fetch_assoc( $results ) ) {
                $ID= $record["ID"];
              	$make = $record['Make'];
              	$model = $record['Model'];
              	$year= $record['Year'];
              	$mileage= $record['Mileage'];
              	print "<tr><td>$ID</td><td>$make</td><td>$model</td><td>$year</td><td>$mileage</td></tr>";
              }
                
              if(isset($_POST["logout"])){
                header("Location: https://92c7c3a7534d4e0ca0a17e756a858779.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/database-sessions/logout.php/");
              }
            ?>
  
          </tbody>
      </table>
    
    
   
  </body>
</html>
