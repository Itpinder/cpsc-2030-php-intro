<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Booking appointments online for checkup</title>
    <!--<link rel="stylesheet" href="common.css">-->
    
    
    <!--BOOTSTRAP LINK -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="body">
      <h1 class="h1 text-center text-primary">Online Appointment Booking System</h1>
      
      <!--MENU ITEMS-->
     <nav>
      <ul class="nav justify-content-center nav-tabs nav-fill row list-unstyled text-center">
        <li class= "nav-item col-lg-3 col-xl-3 col-md-6 col-sm-12 col-xs-12"><a class="nav-link" href="home.html">Home</a></li>
        <li class= "nav-item col-lg-3 col-xl-3 col-md-6 col-sm-12 col-xs-12"><a class="nav-link" href="about.html">About Us</a></li>
        <li class= "nav-item col-lg-3 col-xl-3 col-md-6 col-sm-12 col-xs-12"><a class="nav-link" href="contact.html">Contact Us</a></li>
        <li class= "nav-item col-lg-3 col-xl-3 col-md-6 col-sm-12 col-xs-12"><a class="nav-link" href="references.html">References</a></li>
      </ul>
    </nav>
    
    
    
    
 
    
    <!--LIST OF DOCTORS-->
    <div class="table-responsive"></div>
        <table class="table table-hover table-dark text-center">
          <thead>
            <tr>
              <th scope="col">Doctor Id</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Specialist Of</th>
              <th scope="col">Day available</th>
              <th scope="col">Time available</th>
            </tr>
          </thead>
          <tbody>
              <?php
                $link = mysqli_connect( 'localhost', 'root', 'Sukhman8*' );
                mysqli_select_db( $link, 'bookingAppointments' );
                
                        
                
                
                $results = mysqli_query( $link, 'SELECT * FROM Doctors' );
                
                 while( $record = mysqli_fetch_assoc( $results ) ) {
                    $Id= $record["Id"];
                  	$firstName = $record['First name'];
                  	$lastName = $record['Last name'];
                  	$specialistOf= $record['Specialist of'];
                  	$dayAvailable= $record['Day available'];
                  	$timeAvailable= $record['Time available'];
                  	print "<tr><td>$Id</td><td>$firstName</td><td>$lastName</td><td>$specialistOf</td><td>$dayAvailable</td><td>$timeAvailable</td></tr>";
                 }
              
              ?>
          </tbody>
        </table>
    </div>
    <p></p>
    
    
    <!--PATIENT INFO-->
    <form class="needs-validation" novalidate action="finaliseBooking.php" method="POST">
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Please fill in the form below:</h4>
          
            <div class="mb-3">
                <label for="msp">MSP number</label>
                <input type="text" class="form-control" id="msp" name="msp" required>
                <div class="invalid-feedback">
                    Valid MSP number is required
                </div>
            </div>
            
            
            <div class="mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="firstName" name="firstName" required>
                <div class="invalid-feedback">
                    Valid first name is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" required>
                <div class="invalid-feedback">
                    Valid last name is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" required>
                <div class="invalid-feedback">
                    Please enter a valid email addres.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="houseNumber">House number</label>
                <input type="number" class="form-control" id="houseNumber" name="houseNumber" required>
                <div class="invalid-feedback">
                    Valid house number is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="streetNumber">Street number/ Avenue</label>
                <input type="text" class="form-control" id="streetNumber" name="streetNumber" required>
                <div class="invalid-feedback">
                    Valid street number is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="lastName">City</label>
                <input type="text" class="form-control" id="city" name="city" required>
                <div class="invalid-feedback">
                    Valid city name is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="province">Province/Territory</label>
                    <select class="custom-select d-block w-100" id="province" name="province[]" required>
                      <option value="">Choose...</option>
                      <option>Alberta</option>
                      <option>British Columbia</option>
                      <option>Manitoba</option>
                      <option>New Brunswick</option>
                      <option>Newfoundland and Labrador</option>
                      <option>Northwest Territories</option>
                      <option>Nova Scotia</option>
                      <option>Nunavut</option>
                      <option>Ontario</option>
                      <option>Prince Edward Island</option>
                      <option>Quebec</option>
                      <option>Saskatchewan</option>
                      <option>Yukon</option>
                    </select>
                    <div class="invalid-feedback">
                      Please provide a valid province.
                    </div>
            </div>
            
            <div class="mb-3">
                <label for="country">Country</label>
                <select class="custom-select d-block w-100" id="country" name="country[]" required>
                  <option value="">Choose...</option>
                  <option>Canada</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="postalCode">Postal code</label>
                <input type="text" class="form-control" id="postalCode" name="postalCode" required>
                <div class="invalid-feedback">
                    Valid postal code is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="doctorId">Doctor Id</label>
                <input type="number" class="form-control" id="doctorId" name="doctorId" required>
                <div class="invalid-feedback">
                    Valid doctor id is required.
                </div>
            </div>
            
            <div class="mb-3">
                <label for="timeBooked">Time you want to book for</label>
                <input type="text" class="form-control" id="timeBooked" name="timeBooked" required>
                <div class="invalid-feedback">
                    Valid time is required.
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit</button>
      </div>
      
       
    </form>
    
//     <?php
   
//   if(isset($_REQUEST["msp"])){
//         $safe_msp = mysqli_real_escape_string( $link, $_REQUEST["msp"] );
//         $safe_firstName = mysqli_real_escape_string( $link, $_REQUEST["firstName"] );
//         $safe_lastName = mysqli_real_escape_string( $link, $_REQUEST["lastName"] );
//         $safe_email = mysqli_real_escape_string( $link, $_REQUEST["email"] );
//         $safe_houseNumber = mysqli_real_escape_string( $link, $_REQUEST["houseNumber"] );
//         $safe_streetNumber = mysqli_real_escape_string( $link, $_REQUEST["streetNumber"] );
//         $safe_city = mysqli_real_escape_string( $link, $_REQUEST["city"] );
//         // $safe_province = mysqli_real_escape_string( $link, $_REQUEST["province"] );
//         // $safe_country = mysqli_real_escape_string( $link, $_REQUEST["countr"] );
//         $safe_postalCode = mysqli_real_escape_string( $link, $_REQUEST["postalCode"] );
        
        
//          $query = "INSERT INTO Patients ( Id, FirstName, LastName, Email, HouseNumber, StreetNumber, City, Province, Country, PostalCode, DoctorID, TimeBooked) VALUES ( '45', 'FGH', 'FDGH', 'GFHD', '435', '435', 'GTR', 'EARTE', 'REW', 'R', '90', '78' )";
//                 mysqli_query( $link, $query);            
//     }
//   ?>
    
   
    
      
      <!--BOOTSTRAP LINKS FOR JAVASCRIPT-->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="form-validation.js"></script>
  </body>
</html>