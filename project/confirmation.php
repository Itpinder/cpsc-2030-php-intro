<?php
   
    $link = mysqli_connect( 'localhost', 'root', 'Sukhman8*' );
    mysqli_select_db( $link, 'bookingAppointments' );  

    $selectedAvailability= $_POST["selection"];
    

   
        $results = mysqli_query( $link, "SELECT * FROM DoctorSchedule WHERE Availability='$selectedAvailability';" );
            
            
        while( $record = mysqli_fetch_assoc( $results ) ) {
            $doctorId= $record["DoctorID"];
        }
            
        $username= $_POST["name"];

        
        
        $safe_doctorId= mysqli_real_escape_string( $link, $doctorId );
        $safe_mspNumber= mysqli_real_escape_string( $link, $username );
        $safe_schedule= mysqli_real_escape_string( $link, $selectedAvailability );
  
        $query = "INSERT INTO AppointmentSchedule ( DoctorId, MspNumber, Schedule ) VALUES ( '$safe_doctorId', '$safe_mspNumber', '$safe_schedule')";
        mysqli_query( $link, $query );
        
         
        $query= "UPDATE DoctorSchedule SET Status = '-1' WHERE Availability = '$selectedAvailability';";
        
        
        
        mysqli_query( $link, $query );


   
    
    $results= mysqli_query( $link, "SELECT AppointmentSchedule.Schedule, Users.FirstName, Users.LastName, Users.MspNumber, Users.Email, Users.HouseNumber, Users.StreetNumber, Users.City, Users.Province, Users.Country, Users.PostalCode FROM ((AppointmentSchedule INNER JOIN Doctors ON AppointmentSchedule.DoctorId = Doctors.DoctorId) INNER JOIN Users ON AppointmentSchedule.MspNumber = Users.MspNumber)" );
    
    while( $record = mysqli_fetch_assoc( $results ) ) {
        $final_mspNumber= $record["MspNumber"];
        $final_patientFirstName= $record["FirstName"];
        $final_patientLastName= $record["LastName"];
        $final_email= $record["Email"];
        $final_houseNumber= $record["HouseNumber"];
        $final_streetNumber= $record["StreetNumber"];
        $final_city= $record["City"];
        $final_province= $record["Province"];
        $final_country= $record["Country"];
        $final_postalCode= $record["PostalCode"];
        $final_schedule= $record["Schedule"];
    }
    
    
    // TO RETRIEVE DOCTOR INFO
    $results= mysqli_query( $link, "SELECT AppointmentSchedule.Schedule, Doctors.FirstName, Doctors.LastName, Users.MspNumber, Users.Email, Users.HouseNumber, Users.StreetNumber, Users.City, Users.Province, Users.Country, Users.PostalCode FROM ((AppointmentSchedule INNER JOIN Doctors ON AppointmentSchedule.DoctorId = Doctors.DoctorId) INNER JOIN Users ON AppointmentSchedule.MspNumber = Users.MspNumber)" );
    while( $record = mysqli_fetch_assoc( $results ) ) {
        $final_doctorFirstName= $record["FirstName"];
        $final_doctorLastName= $record["LastName"];
    }
    
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Appointment Booked</title>
    
    <!--BOOTSTRAP LINK -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
  </head>
  <body>
      
      
      <!--Schedule-->
      <!--Doctor Name-> DoctorId-->
      <!--Patient Info-> Msp-->
      
        <h3 class="d-block justify-content-between text-center mb-3">
            <span class="text-muted">Your booking infomation</span>
        </h3>
        
        <div class="d-flex justify-content-center align-items-center" style="height:400px;">
            
            
            <ul class="list-group mb-3 container-fluid">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Your msp number: </h6>
                    <?php
                        echo $final_mspNumber;
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Your name: </h6>
                    <?php
                        print("$final_patientFirstName $final_patientLastName");
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Your email: </h6>
                    <?php
                        print("$final_email");
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Your address: </h6>
                    <?php
                        print("$final_houseNumber $final_streetNumber $final_city $final_province $final_country $final_postalCode");
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Doctor you booked: </h6>
                    <?php
                        print("$final_doctorFirstName $final_doctorLastName");
                    ?>
                </li>
                
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6 class="my-0">Time you booked for: </h6>
                    <?php
                        print("$final_schedule");
                    ?>
                </li>
              
        </div>
    
    
        <a class="text-center" href="home.html" style= "font-size: 40px;">Back to home page</a>
    <!--BOOTSTRAP LINKS FOR JAVASCRIPT-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>